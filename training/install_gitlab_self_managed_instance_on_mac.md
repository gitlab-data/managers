# Install GitLab Self Managed Instance on Mac

This how-to guide helps Data Analyst onboard to the concept of the GitLab Self-Managed Instance on a Mac laptop. 
[Original Issue for Self-Managed Installation with Images](https://gitlab.com/gitlab-data/analytics/-/issues/5097)

## Get Self-Managed EE Instance License

1. Download and install GitLab - `https://about.gitlab.com/install/` 
1. Click on `Get your free trial` -> Redirects to `https://about.gitlab.com/free-trial/self-managed/`
1. Fill out trial information: first_name, last_name, work_email, company_name, job_title, work_phone_number, country, total_number_of_employees, Required Check box to agree to communications. Click `OK` 
1. The page will refresh and inform that one needs additional instructions in inbox shortly. 
1. In your inbox, you should get a license for the EE version. 


## Install Self-Managed EE Instance 
There are multiple ways to [install the GitLab Self-Managed Instance](https://about.gitlab.com/install/). In this guide, we will be [installing GitLab with Docker](https://docs.gitlab.com/ee/install/docker.html) 

### Docker 
1. Download [Docker Desktop for Mac](https://hub.docker.com/editions/community/docker-ce-desktop-mac/) or alternatively follow [the dbt Guide for Docker](https://about.gitlab.com/handbook/business-ops/data-team/dbt-guide/#configuration). 
1. [Sign up for a Docker ID](https://hub.docker.com/?utm_source=docker4mac_2.2.0.0&utm_medium=account_create&utm_campaign=referral) 
1. Open the Docker Application and login with your docker information. 
1. Open terminal
1. Run `docker --version` to check that it has been installed. (It should return something like `Docker version 19.03.5, build 633a0ea`) 
1. Run the command `docker pull gitlab/gitlab-ee` via [command line](https://hub.docker.com/r/gitlab/gitlab-ce/) - This will install the gitlab-ee self-managed instance to the docker. 
1. Follow the commands in [this guide](https://docs.gitlab.com/omnibus/docker/#set-up-the-volumes-location)
    - Note: Make sure the last line is `gitlab/gitlab-ee:latest` instead of `gitlab/gitlab-ce:latest`
    - Note: for hostname, type `hostname` in terminal and it will tell you your hostname. 
    - Note: rename the `name` . For example `<name>` = `gitlab` in the below image
    - Note: `export GITLAB_HOME=$HOME/<name>`. 

**Example**
If this is your first time running this, or you've cleared the data, this is how you would structure it: 

A. Run the following command in terminal to set up the file structure needed. This is very important!
```
export GITLAB_HOME=$HOME/gitlab
```

B. Run the following command to start and run the docker container with the information in it. 
```
sudo docker run --detach \
  --hostname Kathleens-MBP \
  --publish 443:443 --publish 80:80 \
  --name gitlab \
  --restart always \
  --volume $GITLAB_HOME/config:/etc/gitlab \
  --volume $GITLAB_HOME/logs:/var/log/gitlab \
  --volume $GITLAB_HOME/data:/var/opt/gitlab \
  gitlab/gitlab-ee:latest
```

OR a specific version of gitlab-ee such as [12.9](https://registry.hub.docker.com/r/gitlab/gitlab-ee/tags?page=1&name=12.9) if you're having issues updating the PostGres version in the container. For example, GitLab EE `12.9.10` requires PostGres version `11.11` and above, but GitLab EE `12.9.0` can run on PostGres version `11.7`

```
sudo docker run --detach \
    --hostname Kathleens-MBP \
    --publish 443:443 --publish 80:80 \
    --name gitlab \
    --restart always \
    --volume $GITLAB_HOME/config:/etc/gitlab \
    --volume $GITLAB_HOME/logs:/var/log/gitlab \
    --volume $GITLAB_HOME/data:/var/opt/gitlab \
    gitlab/gitlab-ee:12.9.0-ee.0
```

## Visiting the local webpage for the Self-Managed Instance 
1. After starting a container you can visit `http://localhost`. 
1. It will prompt you to save a password. That password is for the username `root`. This is the account with `Admin Access`
1. Additional user registration can take place on `http://localhost/users/sign_in` link. 

## Add the Trial License 
Follow [this guide](https://docs.gitlab.com/ee/user/admin_area/license.html?mkt_tok=eyJpIjoiTUdZeE9XSmlPV05tT1RsaSIsInQiOiJIVW9NeWl0ZGI0SEhCckpRKzNoUjJ3NmV0ajREVkt0M0FSdWt5cGxtR1lDZ1VlUE52ZEVKejNXZlFXOGxxWndyRTdXbUJiUE1rdmVWRUFVS1owMVBsajd6bWt4VDlWdlhmTHcxbGpWQ0FWWE5TazVKaWFTWHM4cGJ2a3E2d2ZzSCJ9) 

## Additional Docker commands  
1. To start the container, run `sudo docker start <name>` like `sudo docker start gitlab`
1. To stop the container, run `sudo docker stop <name>` like `sudo docker stop gitlab`
1. To remove the container, run `sudo docker run --rm <name>`  or `sudo docker rm <name>` like `sudo docker rm gitlab`
1. To see the Docker logs, run `sudo docker logs -f <name>` like `sudo docker logs -f gitlab`


## Errors 

### File Sharing / Mounting Error 

The below error message signals the need to grant access to docker to certain path locations on your Mac. 

```
docker: Error response from daemon: Mounts denied: 
The paths /data and /logs and /config
are not shared from OS X and are not known to Docker.
You can configure shared paths from Docker -> Preferences... -> File Sharing.
See https://docs.docker.com/docker-for-mac/osxfs/#namespaces for more info.
```

#### Solution - [Set up Volume Location](https://docs.gitlab.com/omnibus/docker/#set-up-the-volumes-location)
1. Run the command `export GITLAB_HOME=$HOME/<name>`, where `<name>` = `gitlab` for example: 
```
export GITLAB_HOME=$HOME/gitlab
```

### Database Outdated Error 

The below error message signals the need to upgrade the PostGres version on your Mac: 
```
Recipe: gitlab::database_migrations[0m
  * bash[migrate gitlab-rails database] action run
    [execute] rake aborted!
              Your current database version is too old to be migrated. You should upgrade to GitLab 11.11.0 before moving to this version. Please see https://docs.gitlab.com/ee/policy/maintenance.html#upgrade-recommendations
              /opt/gitlab/embedded/service/gitlab-rails/lib/tasks/migrate/schema_check.rake:13:in `block in <top (required)>'
              /opt/gitlab/embedded/service/gitlab-rails/lib/tasks/gitlab/db.rake:54:in `block (3 levels) in <top (required)>'
              /opt/gitlab/embedded/bin/bundle:23:in `load'
              /opt/gitlab/embedded/bin/bundle:23:in `<main>'
              Tasks: TOP => db:migrate => schema_version_check
              (See full trace by running task with --trace)
    [0m
    ================================================================================[0m
    [31mError executing action `run` on resource 'bash[migrate gitlab-rails database]'[0m
    ================================================================================[0m
```

#### Check your PostGres version  
1. Check your PostGres version in the Docker Terminal with `postgres -V`. For example, my version was `11.7`, but the instructions require `You should upgrade to GitLab 11.11.0 before moving to this version.`


#### Solution - Upgrade PostGres version 
1. Upgrade PostGres version for the Docker Container by opening the CLI terminal for the docker container, and running the command: 
```
apt-get install -y postgresql postgresql-contrib 11.11
```

Note: If the installation gets interrupted, follow the instructions and run 
```
dpkg --configure -a
```

### View Files in Docker Container 
1. To access GitLab’s configuration file, you can start a shell session in the context of a running container. See more details [here](https://docs.gitlab.com/omnibus/docker/#configuration). 

```
sudo docker exec -it gitlab /bin/bash
```

**Example**
```
sudo docker exec -it gitlab /bin/bash
cd var/opt/gitlab/redis

```


### No such file or directory for Redis 
An example of this issue can be seen below: 

```
    ================================================================================[0m
    [31mError executing action `enable` on resource 'runit_service[redis]'[0m
    ================================================================================[0m
    
[0m    Errno::ENOENT[0m
    -------------[0m
    template[/var/log/gitlab/redis/config] (/opt/gitlab/embedded/cookbooks/cache/cookbooks/runit/libraries/provider_runit_service.rb line 136) had an error: Errno::ENOENT: No such file or directory @ realpath_rec - /opt/gitlab/sv/redis/log/config[0m
    
```

#### Solution 
1. Running the following commands after modifying the container name 
```
docker exec <containername> cp /var/opt/gitlab/redis/redis.conf /opt/gitlab/sv/redis/log/config
docker exec <containername> cp /var/opt/gitlab/gitaly/config.toml /opt/gitlab/sv/gitaly/log/config
docker exec <containername> cp /var/opt/gitlab/postgresql/data/postgresql.conf /opt/gitlab/sv/postgresql/log/config
```

**Example:**
```
docker exec gitlab cp /var/opt/gitlab/redis/redis.conf /opt/gitlab/sv/redis/log/config
docker exec gitlab cp /var/opt/gitlab/gitaly/config.toml /opt/gitlab/sv/gitaly/log/config
docker exec gitlab cp /var/opt/gitlab/postgresql/data/postgresql.conf /opt/gitlab/sv/postgresql/log/config
```

2. You may need to remove any left-over data from any other versions (say you use the latest and 12.9). 
```
`mv $HOME/gitlab $HOME/gitlab-backup` - moves all existing values to a backup folder 
`export GITLAB_HOME=$HOME/gitlab` - reset the GITLAB_HOME values 

`sudo docker run --detach \
     --hostname Kathleens-MBP \
     --publish 443:443 --publish 80:80 \
     --name gitlab \
     --restart always \
     --volume $GITLAB_HOME/config:/etc/gitlab \
     --volume $GITLAB_HOME/logs:/var/log/gitlab \
     --volume $GITLAB_HOME/data:/var/opt/gitlab \
     gitlab/gitlab-ee:12.9.0-ee.0` - recreate the container image
```

Then Wait 5 minutes for things for GitLab to connect all the dots and start all the services (check docker logs to see what's up). 
When you visit http://loca​lh​ost ​in​ yo​ur ​b​ro​w​se​r, it should now work! 



### Additional GitLab Configurations 
Configure GitLab for your system by editing /etc/gitlab/gitlab.rb file
And restart this container to reload settings.
To do it use docker exec:
```
  docker exec -it gitlab vim /etc/gitlab/gitlab.rb
  docker restart gitlab
```

For a comprehensive list of configuration options please see the [Omnibus GitLab readme](https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/README.md)


### Permission Issues 
If this container fails to start due to permission problems try to fix it by executing:
```
  docker exec -it gitlab update-permissions
  docker restart gitlab
```



