# [Name of Project] - Project Plan 

## Project Attributes 

Below is a list of quick links and information for ease of reference. 

| Property               | Value                                                                        |
|------------------------|------------------------------------------------------------------------------|
| Date Created           | YYYY-MM-DD                                                                   |
| Estimated Date Ended   | YYYY-MM-DD                                                                   |
| Slack                  | [#channel]() (only accessible from within the company).                      |
| Google Doc             | [`project` Working Group Agenda]() (only accessible from within the company) |

## Project Vision  

###  Business Goal
<!-- Explain the purpose of this project and how it will achieve a business goal.-->
DRI: <!-- Customer -->

#### Business Opportunity  / Customer Outcome
<!-- Explain the business opportunity that will be gained if the project is a success. -->

#### Problem Statement 
<!-- Explains the current pain points that needs to be addressed.-->

|                                |                                                                              |
|--------------------------------|------------------------------------------------------------------------------|
| The problem of                 | <!--Explains the problem in a sentence          -->                          |
| affects                        | <!--the target audience - customer segmentation     -->                      |
| the impact of which is         | <!--what is the outcome if the problem is not addressed?          -->        |
| A successful solution would be | <!--what is the goal of the solution. how does this address the problem? --> |

#### Position Statement 

| For                            |  <!--target audience -->                                                     |
|--------------------------------|------------------------------------------------------------------------------|
| Who                            | <!--why would this target audience care?          -->                        |
| <!-- The project solution> --> | <!--what type of solution is it     -->                                      |
| That                           | <!--how this particular solution maps to the succesful soluion          -->  |
| Unlike                         | <!--what is current solution and it's pain points                        --> |
| Our solution                   | <!--what is motivation for this solution.                                --> |

### Success Measurements / Exit Criteria 
DRI: <!-- Customer -->

<!-- Explain how we define project success and how we capture the ROI for the business goal.-->


### Scope / Project Objectives 
<!-- Explain the scope of this project, what will be delivered and what is not considered.-->

#### Software Development Life Cycle Description 
<!-- Explain the SDLC life cycle being followed here (Kanban, Scrum, etc...). Explain how meeting cadences work within this SLDC as well as touchpoints.-->

#### Work Products (Epics)
<!-- List the child epics/parent issues that represent the work product-->

#### Milestones
<!-- List the milestones and the current slated work products against this milestone -->

#### Programs Supported 
<!-- List of Enterprise Programs we are supporting -->



### Definitions, Acryonyms, & Abbreviations 

| Word/Phrase           | Acrynoyms / Abbreviations      | Definition |
|-----------------------|--------------------------------|------------|  
| Facilitator           | FC.                            |   Ex.      |


### References 
<!-- Links to existing documents and it's significance for this project.-->


## Project Charter 

### Market Demographics 
<!-- Explanation of the market we are targeting and their demographics-->

### Key Stakeholder Goals/Needs 

| Need                  | Priority              | Concerns         | Current Solution| Proposed Solution| 
|-----------------------|-----------------------|------------------|-----------------|------------------|
| Meets Budget          | `High`                |                  |                 |                  |      
| Meets Compliance      | `High`                |                  |                 |                  |    
| Meets Schedule        | `Medium`              |                  |                 |                  |    
| Ease of Use           | `Medium`              |                  |                 |                  |    

### Roles and Responsibilities 

| Working Group Role    | Person                | Role Description | Responsibilitiy | Success Criteria | Involement | Deliverables | Comments/Issues| 
|-----------------------|-----------------------|------------------|-----------------|------------------|------------|--------------|----------------|
| Facilitator           | `Project Lead`        |                  |                 |                  |            |              |                |

### Team Operating Procedures
This section details how the team's operating procedures. 

#### Team Availability 
<!-- This explains when the members working on this project are available as a reference in case meetings need to be shifted. -->

#### Project Presentation 
<!-- This explains how the members working on this project will be presenting the project work products and who will be responsible. -->

#### Decision-Making Process 
<!-- This explains how the members working on this project will be making decisions throughout the project lifetime. -->

#### Conflict Resolution Process
<!-- This explains how the members working on this project will be deal with conflicting ideas throughout the project lifetime. -->

#### Documentation  
This section lists out the documentations we will need as well as the DRI for each section. 

##### Handbook 
<!-- This section should contain links to the handbook, the handbook MR, and the DRI. -->

##### docs.gitlab.com 
<!-- This section should contain links to the docs.gitlab.com, the associated MR, and the DRI. -->

##### ReadMes 
<!-- This section should contain links to the ReadMe, the associated MR, and the DRI. -->


## Solution Overview 

This section documents the solution for the project and how it achieves the project requirements and overcomes any project constraints. 

### Capabilities Summary 

This section lists out the MVC iterations. 

<!-- This section should either have sections like below or links to issues that have information as documented below. -->

#### MVC 1 
| Customer Benefit      | Supporting Features            | 
|-----------------------|--------------------------------|
| Benefit 1             | Feature description            |

#### MVC 2 
| Customer Benefit      | Supporting Features            | 
|-----------------------|--------------------------------|
| Benefit 1             | Feature description            |

### Solution Assumptions, Dependencies, and Caveats  
<!-- Information on if there are limitations to the solution -->

### Solution Cost and Pricing 
<!-- Information on if there are cost/pricing to the solution -->

### Solution Features 
<!-- Information on if there are features to the solution -->

### Solution Contraints  
<!-- Information on if there are constraints to the solution -->

## Quality Ranges 
This section explains how we will check our data and platform quality.

### Availability 
<!-- Explain Data and Platform Availability Schedules and how this can impact how we view data quality -->

### Usability 
<!-- Explain how users will interact with the data/platform and how we will judge the quality of the usability of the work products. -->


### Maintainability 
<!-- Explain how the Data team will maintain both the data sets and the platform to ensure that it is up-to-date and is scalable. -->

### Fault Tolerance (Optional)
<!-- Explains the fault tolerance that we as a company agree to  -->


### System Requirements 
This section lists out the system requirements that the solution will need to take into consideration 

#### Sisense (Formerly Periscope Data) Requirements 
<!-- Explains Sisense limitations or features we can expand on  -->
* [ ] The final data models are surfaced to the periscope user for querying in Sisense 

#### Snowflake Requirements 
<!-- Explains Snowflake limitations or features we can expand on  -->


#### dbt Requirements 
<!-- Explains dbt limitations or features we can expand on  -->


### Performance Requirements 
<!-- This section lists out the performance requirements of the solution. Example for Sisense below  -->

#### Sisense (Formerly Periscope Data) Requirements 
* [ ] The final data models being queried in the Sisense space can be executed in < 2 minutes on XS warehouse. 


## Precedence and Priority 

This section details all the features that will be released as part of the solution, as well as the prioritization of the features below. 

### Critical Priority Features 
<!-- This section lists out the feature requirements that are must have. The project cannot be closed unless these specific features are delivered.-->

### High Priority Features 
<!-- This section lists out the feature requirements that are high priority. The project should consider the delivery of these features first after all critical features have been delivered.-->

### Medium Priority Features
<!-- This section lists out the feature requirements that are medium priority. The project should explain how much of this is in scope for this project.--> 

### Low Priority Features 
<!-- This section lists out the feature requirements that are low priority. The project should explain how much of this will likely fall to a future project. --> 


## Other Solution Requirements 

### Compliance Requirements 
<!-- This section lists out the compliance requirements that must be achieved. This should result in issue(s) that capture how the solution has checked off these requirements-->

### Security Requirements 
<!-- This section lists out the security requirements that must be achieved. This should result in issue(s) that capture how the solution has checked off these requirements-->

### Legal Requirements 
<!-- This section lists out the legal requirements that must be achieved. This should result in issue(s) that capture how the solution has checked off these requirements-->

#### Privacy Requirements 
<!-- This section lists out the legal - privacy requirements that must be achieved. This should result in issue(s) that capture how the solution has checked off these requirements-->

#### Contractual / License Requirements 
<!-- This section lists out the legal - contract requirements that must be achieved. This should result in issue(s) that capture how the solution has checked off these requirements-->

### Data Goveranance Requirements
<!-- This section lists out the data governance requirements that must be achieved. This should result in issue(s) that capture how the solution has checked off these requirements-->



## Risk Assessment Plan 

Risks are inherent in every engineering project. A risk is defined as either "the possibility of loss" or as "a potential problem". Every risk has a probability or likeliness to occur as well as a consequence for that risk if it does occur. In order to stay within project constraints, it is important to have a Risk Management plan ready to address any risk or its consequences that may come.  

Below is the plan for Risk Management, which explains how we will identify, analyze, track, report, and resolve risk issues. We will also examine the methods and tools with which we plan to use to track and record the risk, its impact, its status, and its consequences throughout the project lifecycle. 

Being aware of the fact that risks to any project are dynamic, we know that the tracking and managing of risk will be a continuous effort throughout the duration of the project life cycle. As with such, we account for time in our schedule for additional risk and the potential for rework, and we also account into our schedule the small possibility that some risk may not occur while some will.  

We start the Risk Management process by identifying all possible risks that could occur on the project. To do so, we must remember the following places in which risk resides:  
*  Uncertainty – there is inherent risk in project artifacts that we are uncertain of, such as requirements that can be ambiguous or unclear. Similarly, poor communication between stakeholders and poor understanding of deliverables could result in more risk due to uncertainty.  
*  Knowledge – there is inherent risk in what we don't know. Just because we cannot see the risk, does not mean it does not exist. Similarly, the less we know about certain parts of the project presents more risk. For example, if we do not know that we will be doing rework, then this adds more risk to the schedule.   
*  Concerns – "are what we feel uneasy about". Concerns may arise from interpersonal relationship (whether internal, external, or combination), project constraints, schedule changes, or project familiarity (especially with software language).  
*  Issues – requires resolution and increases probability of risk occurring. It may involve people, organizations, or systems.
*  Project/Program Management – these risks are inherent due to the need to manage people or project artifacts as well as any changes decided last minute by any stakeholders on any project artifacts (for example, last minute requirement changes).  
*  Process-Related – these risks are inherent if the process has not been thoroughly and responses have not been documented thoroughly. For example, a poor risk management record or list of poorly constructed requirements. 
*  Product-Related – added complexity in a product introduces additional risk with performance, maintenance, and unclear requirements.  

### Risk Reference Tables  

#### Risk Severity Table 
Risk Severity matrix takes in the inputs of Risk exposure, or the product of the risk probability of occurring and the resulting consequence criterion, against time.

|                   |  Low Risk Exposure  |Medium Risk Exposure |	High Risk Exposure|
|-------------------|---------------------|---------------------|---------------------|
|Short Time	        |  5	              |    2	            |1                    | 
|Medium Time	    | 7	                  | 4	                | 3                   |
|Long Time	        | 9	                  | 8                   |	6                 |

#### Simple Risk Probability Table to Assess Risk Occurence Likelihood 
This table is a simple probability table that can be used to assess risk occurrence likelihood.

| Probability                            | Uncertainty Category           | 
|----------------------------------------|--------------------------------|
| Less than 40% chance of occurring      |Low                             |
| Between 40% and 70% chance of occurring| Medium                         |
| Greater than 70% chance of occurring.  | High                           |

#### Risk Consequence Table 
Risk Severity matrix takes in the inputs of Risk exposure, or the product of the risk probability of occurring and the resulting consequence criterion, against time.

|   Criterion        |  Cost              |Schedule             |
|-------------------|---------------------|---------------------|
| Low Risk	        |  Less than 1%       |    Slip 1 week      |
| Moderate Risk	    |  Less than 5%       | Slip 2 weeks	    | 
| High Risk	        |  More than 5%	      | Slip more than 2 weeks  |	

#### Time Frame Evaluation Criteria
The table below categorizes the time frame into categories.

| Category | Time Frame |
| ------ | ------ |
| Short | 1 day |
| Medium | 1 week | 
| Long | 1 month | 

#### Risk Prevention  
The team will work to create a plan to reduce as many likelihoods of any project risk from occurring.  We plan to include methods that will assist in risk prevention, risk mitigation, risk resolution, and risk closure. An example of a Prevention Table is seen below for specific risk scenarios.  

|  Risk             |  Severity           |Exposure             | Prevention          |
|-------------------|---------------------|---------------------|---------------------|
|Lack of Time	    |  High	              |    High	            |Ensure requirements gathering and planning is as thorough as possible. Budget excess time after final milestone to mitigate the effect of unforeseen delays.                         | 
|Communication	    | Medium	          | Medium	            | Plan weekly status meetings, agree upon communication platform.                        |
| No QA team	    | Low                 | Medium              |	Use Agile; ensure developers are testing their own code                 |



### Roles and Responsibility 

| Role                  | Responsibities                 | 
|-----------------------|--------------------------------|
| Who? What Role?       | Examines risk identity and consequences on all possibly affected systems (both internal and external). Classifies and tracks risk through project lifecycle. |
| Who? What Role?       | Identifies potential risks, its dependencies, and consequences. Estimates risk impact, timing issues, and risk priority. Prioritizes risk. Creates risk mitigation, contingency, and tracking plans.  |
| Who? What Role?       | Communicates to external stakeholders to mitigate risk consequences. Manages communication in risk management.  |
| Who? What Role?       | Assist in the identification and mitigation of risk. Helps analyze the risk environment, impact, consequence, and priority.    |


#### Risk Management Meeting Plan 

*  Risk Management Meeting Schedule - The team will meet every week and discuss any risk that need to be resolved in the future week. We will also meet for emergency situations if necessary by scheduling meetings through email. 

*  Risk Management Retrospective - As a team, we will meet to discuss what we have learned from this project and how we could have mitigated or closed more risks. We will take these lessons onto future projects so that we can be better at risk management.  

### Risk Management Record  
It is important to review all risk via a framework to help business stakeholders understand all the project risk as well as the solution for each risk. 


|Risk	|Probability	|Consequence|	Exposure|	Severity|	Time Frame|	Priority|	Action|	Person performing action|	Time stamp|
|-------|-------|-------|-------|-------|-------|-------|-------|-------|-------|
|       |       |       |       |       |       |       |       |       |       |							

