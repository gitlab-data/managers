# Data Success Plan
The Data Success Plan defines the team operating and data development model to enable a Business Function to succeed with Data, given a centralized Data Team organization design. The plan is developed jointly by the Business Function and the central Data Team, with a goal of enabling the Business Function to solve their long-term Data objectives and continually grow Data Acumen. 

The key parts of the plan include:
* Assessment of the Functional Team's "Current State" Data Capabilities
* Identification of Functional Team's "Target State" Data Capabilities
* Definition and alignment on a Plan to move from Current State to Target State
* Clear roles and responsibilities across team boundaries
* Leveraging the Data Champion model to kick-start Data and quickly develop capabilities
* Ensuring Data solutions conform to [Enterprise  Single Source of Truth Standards](https://about.gitlab.com/handbook/business-ops/data-team/#responsibilities) 

## Overview
* Business Function: Product, Growth
* Business Function Vision:
* Division DRI:
* Department DRI:

## "Current State" Overview
### Top 5 Data Objectives
The following table summarizes the Business Function's top overall Data Objectives, not necessarily those that require the support of the Data Team.

| Priority | Objective | Target Schedule | Notes |
| ------ | ------ | ------ | ------ |
| 1 | | | |


### Data Staffing
This table reflects the current Data staffing of the Business Function, including Embedded Data Analysts from the Data Team and Data staff within the Business Function.

Issue Board:

| Position | # People | Capacity | Time Period | 
| ------ | ------ | ------ | ------ |
| | | | |

### Data Capabilities
This table summarizes the current data capabilities within the Business Function. Capability guidelines are located below.

| Area | Capability Level | Notes |
| ------ | ------ | ------ |
| Analyst Capacity |  |  |
| Report Generation |  |  |
| Descriptive Analysis |  |  |
| Predictive Analysis |  |  |
| Prescriptive Analysis |  |  |
| Journey Analysis |  |  |

### Data Technology Capabilities
This table summarizes the current data technology capabilities within the Business Function. Capability guidelines are located below.

| Technology | Capability Level | Notes |
| ------ | ------ | ------ |
| SQL | | |
| SiSense | | |
| Snowflake | | |
| dbt | | |
| python | | |

### Data Technology License Allocation
This table summarizes the current data technology license allocation to the Business Function.

| Technology | # Licenses | Notes |
| ------ | ------ | ------ |
| Sisense Viewer Licenses Issued |  |  |
| Sisense Creator Licenses Issued |  |  |
| Snowflake Logins Issued |  |  |

## "Close The Gap" Plan
The Close The Gap Plan covers the elements required to help the Business Function achieve its Data Objectives.

### Data Objective Solution Summary
The following table summarizes the Business Function's top overall Data Objectives, not necessarily those that require the support of the Data Team.

| Priority | Objective | Solution | Notes |
| ------ | ------ | ------ | ------ |
| 1 | | | |


### Data Analytics Capabilities Needed

| Area | Solution | Notes |
| ------ | ------ | ------ |
| Analyst Capacity |  |  |
| Report Generation |  |  |
| Descriptive Analysis |  |  |
| Predictive Analysis |  |  |
| Prescriptive Analysis |  |  |
| Journey Analysis |  |  |

### Data Model Capabilities Needed
| Area | Solution | Notes |
| ------ | ------ | ------ |
| Data Model |  |  |


### Technology Capabilities Needed

| Technology | Solution | Notes |
| ------ | ------ | ------ |
| SQL | | |
| SiSense | | |
| Snowflake | | |
| dbt | | |
| python | | |

### Training Plan
| Item | Plan | Due Date |
| ------ | ------ | ------ |
| Enterprise Data Platform  | | |
| Data Roadmap | | |
| Data Subject Training | | |
| Dashboard Training | | |
| SiSense Training | | |
| Snowflake Training | | |

## Capability Model
| Area | New | Experienced | Expert |
| ------ | ------ | ------ |  ------ |
| Analytics | Basic Reporting | Descriptive Analysis | Journey Analysis and Predictive Analysis |
| SQL | Selects, Joins, and Filters | Common Table Expressions, Inner & Outer Joins, Windowing | Data Transforms, including RegEx, 5+ Table Joins, Performance Optimization |
| SiSense | Read-Only | Dashboard Development & Experienced SQL | Enterprise Dashboard & Advanced SQL |
| Data Programming Languages (Python, Ruby, Perl, etc.) | < 1 year shipping production code | 1-5 years shipping production code | 5+ years shipping production code |
| EDW Data Mart Development with dbt | < 1 year shipping production code | 1-2 years shipping production code | 2+ years shipping production code |
| Dimensional Model Design | Created 1-2 Data Marts | Created 1 Production EDM with over 25 objects | Created 2+ Production EDMs over 50 objects |