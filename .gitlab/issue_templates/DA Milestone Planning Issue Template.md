# `YY.MM.DD - DA` Planning 

[Product Release to Adoption (R2A) board](https://gitlab.com/groups/gitlab-data/-/boards/1912130?&label_name[]=ft%3Ar2a) 
[Lead to Cash (L2C) board](https://gitlab.com/groups/gitlab-data/-/boards/1910594?&label_name[]=ft%3Al2c)
[Data Analytics board](https://gitlab.com/groups/gitlab-data/-/boards/1587810?scope=all&utf8=%E2%9C%93&state=opened&milestone_title=YY.MM.DD%20-%20DA%20(current)). 
[DE Current Milestone](https://gitlab.com/groups/gitlab-data/-/boards/1373923?milestone_title=20.07.29%20-%20DE%20(current)&)

Please bookmark and use this board to work from in priority order. 

This issue should be opened at:
- the earliest on the Thursday before the milestone. 
- the latest on the Monday before the milestone. 

--- 

## Announcements
This section contains information on events that could affect several members of the team and the capacity. (Ex: company conferences, world events, etc...)

--- 

## Planning Tasks

### Before the Milestone starts (Before Thursday - Monday)
* [ ] Manager, Data (Analytics) creates milestone planning issue before the Thursday before the milestone week starts
* [ ] Business DRI assigns priority by using the scoped `~P1/P2/P3/P4` labels to issues of interest before the Thursday before the milestone week starts
    - Issue selected must have been through the validation phase of the issue workflow and have the label `~workflow::scheduling`
    - Issue selected must have weights assigned by a Data team member 
    - Issue selected must be properly labelled 
* [ ] Team updates the `Calculate Metrics` section by the Friday before the milestone week starts
* [ ] Team adds a screenshot of the list of issues in the `Screenshot of Commitment before Milestone` column of the `Prioritized Issues` sections  by the Friday before the milestone week starts
* [ ] Team move issues into `~workflow::ready for development` and places the milestone on the issue that they commit to by the Friday before the milestone week starts
* [ ] Business DRI and Team finalize negotiations and makes last minute changes by Monday of the milestone week 

### On the First Day of the Milestone (Wednesday)
* [ ] Engineers begin working on issues in `~workflow::ready for development` and move issues into the correct workflow label throughout milestone

### On the Last Week of the Milestone
* [ ] Team cleans up current milestone planning issue by Monday of the last week of the milestone. 
* [ ] Team fills out `Retrospective` section in current milestone planning issue by Monday of the last week of the milestone. 
* [ ] Manager, Data (Analytics) closes out current milestone planning issue on the last day of the milestone (Tuesday) 

---

## Metric Definition 

| Metric           | Business Definition      | Calculation              | 
|------------------|--------------------------|--------------------------| 
| Number of Available Days | This metric measures the number of days (decimal) that a data team member will be available in a milestone | 10 total days in milestone - (days for leave - OOO, holiday, etc) - (2 days for data triage, meetings, grooming, planning, etc) - (# days in additional meetings) | 
| Total Weight Completed (Previous) | This metric measures the total issue weights that were completed in the previous milestone | [Column: Weight Issue Closed](https://app.periscopedata.com/app/gitlab/659376/WIP_Data_team_milestones_parul) | 
| Capacity (Previous) | This metric represents the total predicted weight completion (number of days*velocity) for the previous milestone. Please update the retrospective section with any observations in trend | Copy number from last planning issue | 
| Velocity (Previous) | This metric represents the total velocity (rounded down to the nearest issue weight) | [Column: Estimated Velocity](https://app.periscopedata.com/app/gitlab/659376/WIP_Data_team_milestones_parul) | 
| Capacity (Current) | This metric represents the total predicted weight completion (number of days*velocity) available during the current milestone | Multiply `# of Available Days (Current)` with the `Velocity (Previous)` | 

## Calculate Metrics 
<!-- in alphabetical order -->

| Data Team member    | # of Available Days (Current) | Total Weight Completed (Previous) | Capacity (Previous) | Velocity (Previous) | Capacity (Current) | 
|---------------------|-------------------------------|-----------------------------------|---------------------|---------------------|--------------------|   
| @derekatwood        |                               |                                   |                     |                     |                    |                               
| @iweeks             |                               |                                   |                     |                     |                    |                       
| @jeanpeguero        |                               |                                   |                     |                     |                    | 
| @kathleentam        |                               |                                   |                     |                     |                    |   
| @ken_aguilar        |                               |                                   |                     |                     |                    |   
| @mpeychet_          |                               |                                   |                     |                     |                    |    
| @pluthra            |                               |                                   |                     |                     |                    |    
| **All Members**     |                               |                                   |                     |                     |                    |                                                  
---

## Prioritized Issues :8ball: 

This section is for adding issues that have been prioritized by either the data team or function groups in priority order. 
Prioritization should be discussed in the comments. Once you have put this milestone on all the issues for this milestone, take a screenshot and post it under your name. 

### Lead to Cash Cycle and People Operations (L2CPO) Fusion Team Issues

| Data team member | Screenshot of Commitment before Milestone | Screenshot of Commitment after Milestone | 
|------------------|-------------------------------------------|------------------------------------------|
| @derekatwood     |                                           |                                          |                               
| @iweeks          |                                           |                                          |                        
| @jeanpeguero     |                                           |                                          |  
| @kathleentam     |                                           |                                          |    
| @ken_aguilar     |                                           |                                          |    
| @mpeychet_       |                                           |                                          |     
| @pluthra         |                                           |                                          |     

### Product Release to Adoption (R2A) Fusion Team Issues

| Data team member | Screenshot of Commitment before Milestone | Screenshot of Commitment after Milestone | 
|------------------|-------------------------------------------|------------------------------------------|
| @derekatwood     |                                           |                                          |                               
| @iweeks          |                                           |                                          |                        
| @jeanpeguero     |                                           |                                          |  
| @kathleentam     |                                           |                                          |    
| @ken_aguilar     |                                           |                                          |    
| @mpeychet_       |                                           |                                          |     
| @pluthra         |                                           |                                          |     

### Additional Data Analytics Team Issues

| Data team member | Screenshot of Commitment before Milestone | Screenshot of Commitment after Milestone | 
|------------------|-------------------------------------------|------------------------------------------|
| @derekatwood     |                                           |                                          |                               
| @iweeks          |                                           |                                          |                        
| @jeanpeguero     |                                           |                                          |  
| @kathleentam     |                                           |                                          |    
| @ken_aguilar     |                                           |                                          |    
| @mpeychet_       |                                           |                                          |     
| @pluthra         |                                           |                                          |  


---


## Retrospective (to be completed during Data Analytics Team meeting before the next milestone)

[Milestone Retrospectives](https://www.scrum.org/resources/what-is-a-sprint-retrospective) are important because they allow for an opportunity to reflect on the work that was done - celebrating the success - while identifying areas of improvement. 
By the end of the retrospective, the team should identify improvements to commit to in the next milestone to improve their experience through the milestones.  


| Data team member | What went well in the milestone           | What could be improved                   | What we commit to improve in the next milestone |
|------------------|-------------------------------------------|------------------------------------------|-------------------------------------------------|
| @derekatwood     |                                           |                                          |                                                 |                            
| @iweeks          |                                           |                                          |                                                 |                       
| @jeanpeguero     |                                           |                                          |                                                 | 
| @kathleentam     |                                           |                                          |                                                 |    
| @ken_aguilar     |                                           |                                          |                                                 |    
| @mpeychet        |                                           |                                          |                                                 |  
| @pluthra         |                                           |                                          |                                                 | 


---

<!-- Autopopulate Issue Properties -->
/milestone %"XX.XX.XX - DA" 

   <!-- DO NOT EDIT BELOW THIS LINE -->

/epic &95

/weight 1

/label ~"Data Team" ~"Documentation" ~"Housekeeping" ~"workflow::start (triage)"
