# Create a Dimensional Schema for Our Warehouse

## Current Limitations / Problems 

1. It is difficult to use the Data Warehouse 
2. It is difficult to iterate on dbt models 
3. It is difficult to communicate about our warehouse

### Ease of use

If you wanted to answer a question about customers it isn't immediately clear where you would go to get customer data. Data about customers exists in a variety of places. The reason it exists in so many places is because our warehouse objects are primarily defined by their source (Salesforce, Zuora, Netsuite, etc.) and by our [KPIs](https://about.gitlab.com/handbook/business-ops/data-team/kpi-index/), neither of which give us entities like customers, even though we do a pretty good job of [defining them](https://about.gitlab.com/handbook/sales/#customers). Given that neither the source data nor the KPIs will likely have all the data I need to create a new KPI or to generate insights outside the scope of current KPIs it becomes difficult to know where to go. This increases delivery times for our customers and creates a suboptimal experience for our analysts

### Complexity of Iterations

If you wanted to change a join from left to inner in an xf table it isn't immediately clear what the consequences of that change would be. Insofar as our warehouse objects are defined by the sources or by KPIs the business logic for combining data ends up being ambiguous because it has been defined in ad hoc ways. For example: There might be two KPIs that use the same source data, but use them in different ways. This leads us to struggle with making confident changes because we can't always be confident that we understand what happens downstream. Analysts can learn this over time, but given the increasing complexity of our dbt models and the growing size of the team, we can't count on that much longer.

### Communicating with Stakeholders

If a test fails or an extraction is behind, we don't have a great way to immediately understand or communicate about which dashboards might be effected. dbt provides the lineage graphs, but they are often tangled and complex, further more, just because a table is upstream of a dashboard or KPI, doesn't mean that it's data is being used in that dashboard or KPI. We often over attribute dependencies for this reason.

## Proposed Solution 

We mention Kimball in some of our resources, but haven't really applied dimensional modeling. Given the fact that we are pretty good at defining abstract business entities like [customers](https://about.gitlab.com/handbook/sales/#customers) and [KPIs](https://about.gitlab.com/handbook/business-ops/data-team/kpi-index/) up front I think Kimball would be a good choice. 

Fundamentally applying Kimball Dimensional Modeling would come down to establishing Facts and Dimension tables in our warehouse. Entities like Customers are Dimensions and KPIs would be our metrics (this isn't exactly the case, but it's close). For example, we could create a revenue fact table for all revenue reporting where the primary key and grain is defined by transactions (collected, booked, etc.). Designing our warehouse in this way will help to alleviate many of the problems detailed above. 

### Ease of use

Dimensional schemas are often described as easy to understand by business users because the tables are defined by business process. Many business users don't have a detailed understanding of the various systems they need data from, but they do often understand the processes they are trying to describe with data. Finding data becomes easier and so does getting the right answer because the complicated transformations are done in advance with these business questions in mind in the definition of reusable dimensions and facts. 

### Abstraction Layer
Besides the ease of use for end users, the abstraction layer is helpful in the case we change source systems. End users can continue to use data without having to fully understand the flow of data from source to warehouse.

### Simplicity of Iterations

Because the warehouse is divided into facts and dimensions the granularity of objects and their relation is much more straight forward. This makes making changes relatively simple. Tables are defined by their primary key and grain such that any change to such becomes explicit. 

### Communicating with Stakeholders

Once we get past xf tables in our current warehouse there is a complex web of dependencies. We should divide our warehouse into three layers in order to clarify how we process our data (and where). 

1. `RAW` - Data has not been transformed
2. `COMMON` - Data has been transformed into facts and dimensions defined by business process
3. `MART` - Data has been further denormalized (facts and dimensions joined) to create wide tables for particular uses

Having these layers helps us clarify what to do and who is responsible and interested when things change or break. For example, if there is a problem in a Sales Mart that originated there, it will be clear that only those using that mart will be affected. If there is a problem starting with `COMMON` then we need to look into all the downstream marts and make a more general announcement, but we know that the fix will be in dbt in the first transformation layer. If there is a problem with `RAW` we know that the effects are likely diverse and in our extraction. 

This also helps us understand the severity and potential impact of changes, which is nice. 

---

It is not the case that a dimensional schema will solve all of our problems, and we will likely have need to deviate from it at times. But deviation should be an exception in order to keep clarity in the use and development of the warehouse.

Included in the OKR should be a full scope iteration of this schema for both ARR and Retention Metrics 